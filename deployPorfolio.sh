#!/bin/bash

echo ======== DEPLOY Portfolio : INÍCIO ========

echo compactando arquivos de recursos html, css, js...
echo 
rm ./portfolio.tar.bz2
tar -cvjf portfolio.tar.bz2 ./*
echo 
echo fim da compactacao dos arquivos de recursos

echo Subindo portfolio.tar.bz2...
echo 
sshpass -vp zz4115 scp /media/renato/Dados/Desenv/Portfolio/portfolio.tar.bz2 root@vps6174.publiccloud.com.br:/home/renato/portfolio.tar.bz2
echo 
echo Fim upload portfolio.tar.bz2

echo Conectando ao servidor da Locaweb...
sshpass -p zz4115 ssh -o StrictHostKeyChecking=no root@vps6174.publiccloud.com.br << EOF_OF_COMMAND
  echo Parando serviço nginx
  service nginx stop

  echo excluindo versao antiga
  cd /usr/share/nginx/html
  rm -R portfolio

  echo preparando arquivo para descompactacao
  cd  /home/renato
  rm -R portfolio
  mkdir portfolio
  mv portfolio.tar.bz2 ./portfolio
  cd portfolio

  echo descompactando arquivo portfolio.tar.bz2
  tar -xvjf portfolio.tar.bz2

  echo excluindo arquivo compactado
  rm ./portfolio.tar.bz2

  echo movendo arquivo descompactado para o diretorio do servidor nginx
  mv /home/renato/portfolio /usr/share/nginx/html/portfolio

  echo Iniciar serviço nginx
  service nginx start
EOF_OF_COMMAND

echo ======== DEPLOY Portfolio : FIM ========